package com.fool.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Employee {
    private int id;
    private String lastName;
    private String email;
}
