package com.fool.controller;

import jakarta.annotation.Resource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Locale;

@Controller
public class I18nController {
    private static final Logger logger = LoggerFactory.getLogger(I18nController.class);

    @Resource
    private ResourceBundleMessageSource messageSource;

    @RequestMapping("/localeChange")
    public String localeChange(Locale locale) {
        String user_list = messageSource.getMessage("user_list", null, locale);
        logger.error("user_list:" + user_list + "," + locale.getDisplayName());
        return "redirect:/emp/list";
    }
}

