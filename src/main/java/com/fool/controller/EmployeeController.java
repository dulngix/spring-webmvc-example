package com.fool.controller;

import com.fool.model.Employee;
import com.fool.service.EmployeeService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Map;

@Controller
public class EmployeeController {
    private static final Logger logger = LoggerFactory.getLogger(EmployeeController.class);

    @Autowired
    private EmployeeService employeeService;

    @RequestMapping(value="/emp/list",method= RequestMethod.GET)
    public String list(@RequestParam(required=false,defaultValue="1",value="page") Integer page, Model model) {
        if (page == null || page <= 0) page = 1;
        int size = 5;
        PageHelper.startPage(page, size);
        List<Employee> employees = employeeService.getEmployees();
        PageInfo<Employee> pageInfo=new PageInfo(employees, size);
        model.addAttribute("pageInfo", pageInfo);
        logger.error("page:" + page + ", size:" + size + ", listsize:" + employees.size());
        return "emp/list";
    }

    @RequestMapping(value="/emp/add")
    public String add(String name, String email){
        if (name != null && email != null) {
            for(int i=1; i<=10; i++) {
                employeeService.addEmployee(name + i, email + i);
            }
            logger.error("TEST:" + name + "," + email);
        }
        return "emp/add";
    }

}
