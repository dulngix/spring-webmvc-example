package com.fool.controller;

import com.fool.model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;
import java.util.List;

@Controller
public class HelloController {
    private static final Logger logger = LoggerFactory.getLogger(HelloController.class);

    @RequestMapping("/user/list")
    public String list(Model model) {
        List<User> userList = new ArrayList<>();
        for(int i=1; i<10; i++) {
            User user = new User();
            user.setId(i);
            user.setName("NAME" + i);
            user.setAge(i*10);
            userList.add(user);
        }
        model.addAttribute("userList", userList);
        logger.error("TEST:" + userList);
        return "user/list";
    }
}
