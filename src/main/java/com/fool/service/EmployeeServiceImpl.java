package com.fool.service;

import com.fool.dao.EmployeeMapper;
import com.fool.model.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeeServiceImpl implements EmployeeService {
    public EmployeeServiceImpl() {
        System.out.printf("init EmployeeServiceImpl");
    }

    @Autowired
    private EmployeeMapper employeeMapper;

    @Override
    public Employee getEmployee(int id) {
        return employeeMapper.getEmployeeById(id);
    }

    public List<Employee> getEmployees() {
        return employeeMapper.getAllEmployees();
    }

    @Override
    public void addEmployee(String name, String email) {
        Employee emp = new Employee();
        emp.setLastName(name);
        emp.setEmail(email);
        employeeMapper.insert(emp);
    }
}
