package com.fool.service;

import com.fool.model.Employee;

import java.util.List;

public interface EmployeeService {
    Employee getEmployee(int id);

    List<Employee> getEmployees();

    void addEmployee(String name, String email);
}
