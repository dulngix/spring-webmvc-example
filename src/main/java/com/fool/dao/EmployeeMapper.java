package com.fool.dao;

import com.fool.model.Employee;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

public interface EmployeeMapper {
    @Select("select id,lastName,email from employee where id=#{id}")
    Employee getEmployeeById(int id);

    @Select("select * from employee")
    List<Employee> getAllEmployees();

    @Insert("insert into employee (last_name, email) values (#{lastName}, #{email})")
    void insert(Employee employee);
}
